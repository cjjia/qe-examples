#!/bin/bash -l

#SBATCH -p debug
#SBATCH -N 1
#SBATCH -n 32
#SBATCH -t 0:30:00
#SBATCH -J Ferro
#SBATCH -C haswell
#SBATCH --qos=premium

cd $SLURM_SUBMIT_DIR
module load espresso
echo $PSEUDO_DIR
echo $TMP_DIR

export OMP_NUM_THREADS=1

for i in {28..40}
do
   srun -n 32 pp.x < homo_band_$i.in > homo_band_$i.out
   srun -n 1 ~/MyCodes/espresso-5.1.2/PP/src/pp.x < xsf_band_$i.in > xsf_band_$i.out
done